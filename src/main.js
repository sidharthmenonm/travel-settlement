import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";
import { Datetime } from 'vue-datetime'

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import Vue from 'vue'
import App from './App.vue'
import router from './router'

import '../node_modules/vue-datetime/dist/vue-datetime.css'

Vue.config.productionTip = false
Vue.use(require('vue-moment'));
Vue.component('datetime', Datetime);


window.$ = window.jQuery = jQuery;
window.bootstrap = bootstrap;
window.popper = popper;

window.pdfMake = pdfMake;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')